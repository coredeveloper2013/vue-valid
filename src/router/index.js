import Vue from 'vue'
import Router from 'vue-router'
import bulma from 'bulma/bulma.sass'
import Welcome from '@/components/Welcome'
import CardCollection from '@/components/CardCollection'
import Review from '@/components/Review'
import Monitoring from '@/components/Monitoring'

Vue.use(bulma)
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome
    },
    {
      path: '/CardCollection',
      name: 'CardCollection',
      component: CardCollection
    },
    {
      path: '/Review',
      name: 'Review',
      component: Review
    },
    {
      path: '/Monitoring',
      name: 'Monitoring',
      component: Monitoring
    }
  ]
})
